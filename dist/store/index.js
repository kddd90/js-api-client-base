"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var vuex_1 = __importDefault(require("./vuex"));
var pinia_1 = __importDefault(require("./pinia"));
exports.default = {
    VuexStore: vuex_1.default,
    PiniaStore: pinia_1.default
};
