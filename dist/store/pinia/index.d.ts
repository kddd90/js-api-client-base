import BaseStore from "../_base_store";
import { IStore, PiniaStoreDefinition, PiniaModuleWrapper } from "../../types";
declare class Store extends BaseStore implements IStore {
    utils: any;
    constructor(props?: any);
    /**
     * Generate the modules dynamically
     * @param {Object} modules
     */
    generateModules(modules: any): any;
    /**
     * Prepare the modules dynamically
     * @param {Object} modules
     */
    useModule(module: PiniaStoreDefinition): PiniaModuleWrapper;
    types(types: object): {};
    /**
     * Generate a state object
     *
     * @param {Object} state
     * @param {boolean} exclusive
     * @returns
     */
    state(state?: object, exclusive?: boolean): () => {
        constructor: Function;
        toString(): string;
        toLocaleString(): string;
        valueOf(): Object;
        hasOwnProperty(v: string | number | symbol): boolean;
        isPrototypeOf(v: Object): boolean;
        propertyIsEnumerable(v: string | number | symbol): boolean;
        config?: undefined;
        status?: undefined;
        appendData?: undefined;
        data?: undefined;
        all?: undefined;
        imported?: undefined;
        exported?: undefined;
    } | {
        constructor: Function;
        toString(): string;
        toLocaleString(): string;
        valueOf(): Object;
        hasOwnProperty(v: string | number | symbol): boolean;
        isPrototypeOf(v: Object): boolean;
        propertyIsEnumerable(v: string | number | symbol): boolean;
        config: {
            index: null;
            form: null;
        };
        status: {
            data: string;
        };
        appendData: boolean;
        data: {
            data: never[];
        };
        all: never[];
        imported: {
            data: never[];
        };
        exported: {
            data: never[];
        };
    };
    /**
     * Generate the getters for the store
     * @aram {Object}
     * @param {boolean} exclusive
     * @returns
     */
    getters(getters: object, exclusive?: boolean): {
        constructor: Function;
        toString(): string;
        toLocaleString(): string;
        valueOf(): Object;
        hasOwnProperty(v: string | number | symbol): boolean;
        isPrototypeOf(v: Object): boolean;
        propertyIsEnumerable(v: string | number | symbol): boolean;
        currentConfig?: undefined;
        indexConfig?: undefined;
        formConfig?: undefined;
        currentData?: undefined;
        isAllLoaded?: undefined;
        lastImported?: undefined;
        log(): any;
    } | {
        constructor: Function;
        toString(): string;
        toLocaleString(): string;
        valueOf(): Object;
        hasOwnProperty(v: string | number | symbol): boolean;
        isPrototypeOf(v: Object): boolean;
        propertyIsEnumerable(v: string | number | symbol): boolean;
        currentConfig: (state: any) => any;
        indexConfig: (state: any) => any;
        formConfig: (state: any) => any;
        currentData: (state: any) => any;
        isAllLoaded: (state: any) => boolean;
        lastImported: (state: any) => any;
        log(): any;
    };
    /**
     * Generate the actions for the store
     *
     * @param {Object} actions
     * @param {string} type
     * @param {boolean} exclusive
     * @returns
     */
    actions(actions: object, _type?: string, exclusive?: boolean): {
        constructor: Function;
        toString(): string;
        toLocaleString(): string;
        valueOf(): Object;
        hasOwnProperty(v: string | number | symbol): boolean;
        isPrototypeOf(v: Object): boolean;
        propertyIsEnumerable(v: string | number | symbol): boolean;
        type: () => string;
        log: () => any;
        api: () => any;
    };
}
export default Store;
