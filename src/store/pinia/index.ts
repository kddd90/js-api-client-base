const _ = require("lodash");
import utils from ".././utils";
import BaseStore from "../_base_store";
import { IStore, IPiniaStoreState, PiniaStoreDefinition, PiniaModuleWrapper } from "../../types";

class Store extends BaseStore implements IStore {
  utils: any;

  constructor(props: any = {}) {
    super(props);
    this.utils = utils;
  }

  /**
   * Generate the modules dynamically
   * @param {Object} modules
   */
  generateModules(modules: any) {
    const allModules: any = {};
    Object.keys(modules).map((module: string) => {
      allModules[module] = this.useModule(modules[module]);
    });
    return allModules;
  }

  /**
   * Prepare the modules dynamically
   * @param {Object} modules
   */
  useModule(module: PiniaStoreDefinition) {
    const clone = _.clone(this, true);
    const moduleObject: PiniaModuleWrapper = {
      store: module(clone)(),
      log: this.log,
      app: this.app,
      api: this.api,
    };
    return moduleObject;
  }

  types(types: object) {
    return {};
  }

  /**
   * Generate a state object
   *
   * @param {Object} state
   * @param {boolean} exclusive
   * @returns
   */
  state(state: object = {}, exclusive: boolean = false) {
    const extend = state instanceof Object ? state : {};
    const baseState =
      exclusive === true
        ? {}
        : {
          config: {
            index: null,
            form: null,
          },
          status: {
            data: ``,
          },
          appendData: false,
          data: {
            data: [],
          },
          all: [],
          imported: {
            data: [],
          },
          exported: {
            data: [],
          },
        };
    return () => ({
      ...baseState,
      ...extend
    });
  }

  /**
   * Generate the getters for the store
   * @aram {Object}
   * @param {boolean} exclusive
   * @returns
   */
  getters(getters: object, exclusive: boolean = false) {
    const extend = getters instanceof Object ? getters : {};
    const baseGetters =
      exclusive === true
        ? {}
        : {
          currentConfig: (state: any) => state.config,
          indexConfig: (state: any) => state.config.index,
          formConfig: (state: any) => state.config.form,
          currentData: (state: any) => state.data,
          isAllLoaded: (state: any): boolean =>
            state.data instanceof Object &&
            state.data.data instanceof Array &&
            state.data.data.length > 0,
          lastImported: (state: any) => state.imported,
        };
    const $log = this.$log;
    return {
      ...{
        log() {
          return $log;
        },
      },
      ...baseGetters,
      ...extend,
    };
  }

  /**
   * Generate the actions for the store
   *
   * @param {Object} actions
   * @param {string} type
   * @param {boolean} exclusive
   * @returns
   */
  actions(
    actions: object,
    _type: string = "unknown",
    exclusive: boolean = false
  ) {
    const api = this.api();
    const log = this.log();
    let type: string = _type;
    type = type[0] + type.toString().substring(1);
    const extend = actions instanceof Object ? { ...actions } : {};
    const baseActions =
      exclusive === true
        ? {}
        : {
          ...{
            /**
             * Get the index page config for the given type
             * @param {IPiniaStoreState} this
             * @param {Object} params
             * @param {boolean} force
             * @returns {Promise}
             */
            async getIndexConfig(
              this: IPiniaStoreState,
              params: any = {},
              force: boolean = false
            ) {
              const forceGet = force || true;
              try {
                if (!this.config.index || forceGet) {
                  log.info(`[Store: ${type}]: GetIndexConfig`);
                  const response = await api.getIndexConfig(params);
                  this.config.index = response.data.data;
                  return this.config.index;
                } else {
                  log.info(
                    `[Store: ${type}]: Getting existing index config`,
                    params
                  );
                  return this.config.index;
                }
              } catch (error: any) {
                log.info(`[Store: ${type}]: Error getting index config`, error);
                throw new Error(error);
              }
            },
            /**
             * Get the form config needed for creating or updating models
             * @param {IPiniaStoreState} this
             * @param {object} params
             * @param {boolean} force
             * @returns {Promise}
             */
            async getFormConfig(
              this: IPiniaStoreState,
              params: any = {},
              force: boolean = false
            ) {
              try {
                const forceGet = force || true;
                if (!this.config.form || forceGet) {
                  log.info(`[Store: ${type}]: GetFormConfig`);
                  const response = await api.getFormConfig(params);
                  this.config.form = response.data.data;
                  return this.config.form;
                } else {
                  log.info(
                    `[Store: ${type}]: Getting existing form config`,
                    params
                  );
                  return this.config.form;
                }
              } catch (error: any) {
                log.info(`[Store: ${type}]: Error getting form config`, error);
                throw new Error(error);
              }
            },
            /**
             * Set the ability to append data to existing data
             * @param {IPiniaStoreState} this
             * @param {Object} params
             * @returns {Promise}
             */
            async setAppendsData(
              this: IPiniaStoreState, params: boolean) {
              log.info(`[Store: ${type}]: Set Appends Data ${type}`, params);
              this.appendData = !!params;
              return this.appendData;
            },
            /**
             * Get all of the items
             * @param {IPiniaStoreState} this
             * @param {Object} params
             * @returns {Promise}
             */
            async getAll(
              this: IPiniaStoreState, params: any = {}) {
              log.info(`[Store: ${type}]: Get ${type}`, params);
              try {
                const response = api.getAll(params);
                log.info(
                  `[Store: ${type}]: Got all ${type}`,
                  response.data
                );
                if (this.appendData) {
                  this.data = {
                    ...response.data,
                    data: {
                      data: this.data.data.concat(response.data),
                    },
                  };
                } else {
                  this.data = response.data;
                }
                //   state.all = state.all.concat(data.result.data);
                return this.data;
              } catch (error: any) {
                log.info(`[Store: ${type}]: Error getting all`, error);
                throw new Error(error);
              }
            },
            /**
             * Set the data for the given type
             * @param {IPiniaStoreState} this
             * @param {any} data
             */
            async setAll(
              this: IPiniaStoreState, data: any = {}) {
              log.info(`[Store: ${type}]: Set data ${type}`, data);
              this.data = data;
              this.all = this.all.concat(data);
              return this.all;
            },
            /**
             * Get the specific object with the given id
             * @param {IPiniaStoreState} this
             * @param {number|string} id
             * @returns {Promise}
             */
            async getOne(
              this: IPiniaStoreState, id: any = {}) {
              log.info(`[Store: ${type}]: Get ${type}`, id);
              try {
                log.info(`[Store: ${type}]: Getting ${type}`, id);
                if (id) {
                  const response = await api.getOne(id);
                  const result = response.data.hasOwnProperty("meta")
                    ? {
                      meta: response.data.meta,
                      data: response.data.data,
                    }
                    : response.data;
                  utils.addToStateData(this.data.data, result, true);
                  return result;
                } else {
                  return {};
                }
              } catch (error: any) {
                log.info(`[Store: ${type}]: Error getting one`, error);
                throw new Error(error);
              }
            },
            /**
             * Set the given object in the local store
             * @param {IPiniaStoreState} this
             * @param {any} data
             * @returns {Promise}
             */
            async setOne(
              this: IPiniaStoreState, data: any = {}) {
              log.info(`[Store: ${type}]: Set one ${type}`, data);
              utils.addToStateData(this.data.data, data, true);
              return data;
            },
            /**
             * Get the specific object with the given id in the lcoal cache
             * @param {IPiniaStoreState} this
             * @param {number|string} id
             * @returns {Promise}
             */
            async getOneCached(
              this: any, id: any) {
              log.info(`[Store: ${type}]: GetOneCached`, id);
              if (utils.findItemInState(this.data.data, id, true) === -1) {
                return this.getOneCached(id);
              } else {
                log.info(`[Store: ${type}]: Getting existing ${type}`, id);
                return utils.getItemInState(this.data.data, id, true);
              }
            },
            /**
             * Save the given data to the store
             * @param {IPiniaStoreState} this
             * @param {Object} params
             * @returns {Promise}
             */
            async save(
              this: IPiniaStoreState, params: any) {
              log.info(`[Store: ${type}]: Save ${type}`, params);
              try {
                const response = await api.save(params);
                log.info(`[Store: ${type}]: Saved ${type}`, response);
                const data = response.data;
                utils.addToStateData(this.data.data, data, true);
                return data;
              } catch (error: any) {
                log.info(`[Store: ${type}]: Error saving`, error);
                throw new Error(error);
              }
            },
            /**
             * Duplicate the given data to the store
             * @param {IPiniaStoreState} this
             * @param {Object} params
             * @returns {Promise}
             */
            async duplicate(
              this: IPiniaStoreState, params: any) {
              log.info(`[Store: ${type}]: Duplicate ${type}`, params);
              try {
                const response = api.duplicate(params);
                log.info(
                  `[Store: ${type}]: Duplicated ${type}`,
                  response
                );
                const data = response.data;
                utils.addToStateData(this.data.data, data, true);
                return data;
              } catch (error: any) {
                log.info(`[Store: ${type}]: Error duplicating`, error);
                throw new Error(error);
              }
            },
            /**
             * Import the given data into the store
             * @param {IPiniaStoreState} this
             * @param {Object} params
             * @returns {Promise}
             */
            async import(
              this: IPiniaStoreState, params: any) {
              log.info(`[Store: ${type}]: Import`, params);
              try {
                const response = await api.import(params);
                log.info(`[Store: ${type}]: Imported`, response);
                const data = response.data;
                this.imported = data;
                if (data.data instanceof Array) {
                  this.data.data.push([...data.data]);
                  this.all = this.all.concat(data.data);
                }
                return data;
              } catch (error: any) {
                log.info(`[Store: ${type}]: Error getting all`, error);
                throw new Error(error);
              }
            },
            /**
             * Export the given data into the store
             * @param {IPiniaStoreState} this
             * @param {Object} params
             * @returns {Promise}
             */
            async export(
              this: IPiniaStoreState, params: any) {
              try {
                log.info(`[Store: ${type}]: Export`, params);
                const response = await api.export(params);
                log.info(`[Store: ${type}]: Exported`, response);
                const data = response.data;
                this.exported = data;
                return this.exported;
              } catch (error: any) {
                log.info(`[Store: ${type}]: Error getting all`, error);
                throw new Error(error);
              }
            },
            /**
             * Delete the given data from the store
             * @param {IPiniaStoreState} this
             * @param {any} params
             * @returns {Promise}
             */
            async delete(
              this: IPiniaStoreState, params: any) {
              log.info(`[Store: ${type}]: Delete ${type}`, params);
              try {
                if (params) {
                  const response = await api.delete(params);
                  log.info(
                    `[Store: ${type}]: Deleted ${type}`,
                    response.data.data
                  );
                  utils.removeFromStateData(this.data.data, params, true);
                  return response.data.data;
                } else {
                  throw new Error(`[Store: ${type}]: Null params`);
                }
              } catch (error: any) {
                log.info(`[Store: ${type}]: Error getting all`, error);
                throw new Error(error);
              }
            },
            /**
             * Toggle the given data from the store
             * @param {IPiniaStoreState} this
             * @param {any} params
             * @returns {Promise}
             */
            async toggle(
              this: IPiniaStoreState, params: any, attr: string) {
              log.info(`[Store: ${type}]: Toggle ${type}`, params);
              try {
                const response = await api.toggle(params);
                log.info(`[Store: ${type}]: Toggled ${type}`, response);
                const result = response.data.hasOwnProperty("meta")
                  ? {
                    meta: response.data.meta,
                    data: response.data.data,
                  }
                  : response.data;

                utils.addToStateData(this.data.data, result, true);
                return result;
              } catch (error: any) {
                log.info(`[Store: ${type}]: Error getting all`, error);
                throw new Error(error);
              }
            },
          },
        };

    return {
      ...{
        type: function () {
          return type;
        },
        log: function () {
          return log;
        },
        api: function () {
          return api;
        },
      },
      ...baseActions,
      ...extend,
    };
  }
}

export default Store;
