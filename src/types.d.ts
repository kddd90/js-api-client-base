export interface IApi {
	log: any,
	$app: any,
	$api: any,
	app: () => {},
	api: () => {},
	setApi(app: any, headers?: any): any,
	setApp(app: any): any,
	// revocable(method: any): any
}

export interface IStore {
	$log: any,
	$app: any,
	$api: any,
	coreTypes?: object,
	allTypes?: any
}

export interface IPiniaStore {
	state: any,
	getters: any,
	actions: any,
}

export interface IPiniaStoreState {
	data: any,
	config: any,
	all: Array<any>,
	appendData: boolean,
	imported: any,
	exported: any,
	satuts: any,
	[key: string]: any,
}

export interface StateDataInterface {
	data: Array<any>,
	all?: Array<any>,
	total?: number,
}
export interface StateInterface {
	data: StateDataInterface
	[key: string]: any
}

export interface PiniaModuleWrapper {
	store: PiniaStoreDefinition,
	log: any,
	app: any,
	api: any,
}

/**
 * Return type of `defineStore()`. Function that allows instantiating a store.
 * From pinia.d.ts
 */export interface PiniaStoreDefinition {
	/**
	 * Returns a store, creates it if necessary.
	 *
	 * @param pinia - Pinia instance to retrieve the store
	 * @param hot - dev only hot module replacement
	 */
	(pinia?: any | null | undefined, hot?: any): any;
	/**
	 * Id of the store. Used by map helpers.
	 */
	$id: any;
}

export interface IStoreOptions extends Object {
	app: any,
	api: IApi,
	logger: any
}

export interface IJsApi {
	app: any,
	api: IJsApi,
	log: any
}